const Message = {
    InvalidMessageType: {type: "InvalidMessage", message: "Invalid Message Type!"},
    AuthConfirmation: {type: "AuthConfirmation", message: "Authentication Confirmation Message"},
    AuthRequest: {type: "RequestAuthentication", message: "Please send an authentication message!"},
    LoginRequest: {type: "AuthenticationRequest", message: "Attempting to login"},
}

function StatusSocket (address, username, password, verbose = false, debug = false) {
    //Constructor Arguments
    this.debug = debug;
    this.verbose = verbose;
    this.address = address;
    this.user = {
        username: username,
        password: password
    }

    //Create a websocket.
    this.socket = new WebSocket(this.address);

    //Key->Value pair to store message -> handler relations
    this.messageHandlers = {};

    //Generic WebSocker opening event
    this.socket.onopen = (event) => {
        if(this.verbose) console.log("Socket opened!");
    }

    //Generic Socket incoming message event
    this.socket.onmessage = (event) => {
        try{ var message = JSON.parse(event.data); }
        catch(err) {console.error("Failed to Parse message!"); return;}
        if(this.debug) console.log("Recieved Message: ", message);
        if(message.type != undefined && this.messageHandlers[message.type] != undefined){
            this.messageHandlers[message.type](message);
        }else{
            if(this.verbose) console.error("No Handler for message: " + message);
        }
    }

    //Generic Socket closed event
    this.socket.onclose = (event) => {
        if(this.verbose) console.log("Websocket Closed!");
    }

    //Register a new message type handler.
    this.onMessage = (messageType, messageHandler) => {
        this.messageHandlers[messageType] = messageHandler;
    }

    //Send a message of type messageType
    this.sendMessage = (messageType, messageObject) => {
        messageObject.type = messageType;
        if(this.debug) console.log("Sending Message: ", messageObject);
        var message = JSON.stringify(messageObject);
        this.socket.send(message);
    }

    // ==== Authentication & predefined message handlers ====
    //Register an AuthenticationConfirmation event handler.
    this.onMessage(Message.AuthConfirmation.type, (message) => {
        if(message.authenticated){
            if(this.verbose) console.info("Successfully Authenticated!");
        }else{
            if(this.verbose) console.warn("Not Authenticated!");
        }
    });
    
    //Handle InvaludMessage message type.
    this.onMessage(Message.InvalidMessageType.type, (message) => {
        if(this.verbose) console.error("Unknown message type! -> " + message.message);
    });

    //Handle AuthRequest message type.
    this.onMessage(Message.AuthRequest.type, (message) => {
        if(this.debug) console.log("Sending Authentication Message...");
        this.sendMessage(Message.LoginRequest.type, {user: this.user});
    })
}