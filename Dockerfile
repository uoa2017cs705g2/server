# Pull base image.
FROM ubuntu

RUN apt-get update

# Install MySQL.
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y mysql-server
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y mysql-client

# Install Node.js
RUN apt-get install --yes curl
RUN curl --silent --location https://deb.nodesource.com/setup_8.x | bash -
RUN apt-get install --yes nodejs
RUN apt-get install --yes build-essential


# Expose ports.
EXPOSE 3306
EXPOSE 3000
EXPOSE 80

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
COPY package.json .

RUN npm install

# Bundle app source
COPY . .

EXPOSE 3306

#Start command.
CMD ["npm", "start"]