const User = require('./models/user');

const Message = {
    InvalidMessageType: {type: "InvalidMessage", message: "Invalid Message Type!"},
    AuthConfirmation: {type: "AuthConfirmation", message: "Authentication Confirmation Message"},
    AuthRequest: {type: "RequestAuthentication", message: "Please send an authentication message!"},
    LoginRequest: {type: "AuthenticationRequest", message: "Attempting to login"},
    StatusUpdate: {type: "StatusUpdate", message: "A relaton has updated their status"},
}

/**
 * Message Handles Array.
 * Maintains a collection of methods to handle message types
 * Format: messageType : { requiresAuth: boolean, handle: function(socket, message)}
 */
var messageHandlers = {};

var userClientPointers = {};

/**
 * Attach this message handler to the socket server
 * @param {Socket Server} wss 
 */
function setupWebsocket(wss){
    wss.on('connection', function connection(ws, req) {
        console.log("Someone Connected! Total Clients: " + wss.clients.size);

        //Send a request for Authentication to the client.
        ws.send(JSON.stringify(Message.AuthRequest));
        
        //Handle client disconnect.
        ws.on('close', function close() {
            console.log('Client Disconnected');
        });

        ws.json = (object) => {
            ws.send(JSON.stringify(object));
        }

        ws.sendMessage = (type, object) => {
            object.type = type;
            ws.json(object);
        }

        //Generic handle incoming message on socket.
        ws.on('message', function incoming(message) {
            try{
                var message = JSON.parse(message);
            }catch(err){
                ws.send(JSON.stringify(Message.InvalidMessageType));
                console.log("Failed to Parse message from socket.");
                return;
            }

            //Call specific message handler for message type defined as message.type
            if(message.type != undefined && messageHandlers[message.type.toLowerCase()] != undefined){
                var handle = messageHandlers[message.type.toLowerCase()];
                if(handle.requiresAuth){
                    if(ws.authenticated){
                        handle.handle(ws, message);
                    }else{
                        sendAuthReply(ws, false); 
                    }
                }else{
                    handle.handle(ws, message);
                }
            }else{
                ws.send(JSON.stringify(Message.InvalidMessageType));
            }
        });
    });

    //Register the local message type for auth
    registerListener(Message.LoginRequest.type, false, (ws, message) => {
        if(message.user != undefined && message.user.username != undefined && message.user.password != undefined){
                //Attempt Login
                User.login(message.user.username, message.user.password, (err, user) => {
                    if(user){
                        sendAuthReply(ws, true); 
                        ws.user = user;
                        ws.authenticated = true;
                        userClientPointers[ws.user.id] = ws;
                    }else{
                        sendAuthReply(ws, false); 
                    }
                })
            }else{
                sendAuthReply(ws, false); 
            }
    });
}

/**
 * Sends generic Auth meessage based upon response. If true, then send true, else send false and close connection.
 * @param {WebSocket connection} ws 
 * @param {Boolean} response 
 * @param {*} done 
 */
function sendAuthReply(ws, response, done){
    ws.send(JSON.stringify({
        type: Message.AuthConfirmation.type,
        authenticated: response
    }));
    if(response == false){
        ws.close();
    }
    if(done){
        done();
    }
}

/**
 * Register a new message type handler
 * @param {String} messageType 
 * @param {Boolean} requiresAuth 
 * @param {function(websocket, message)} handler 
 */
function registerListener(messageType, requiresAuth, handler) {
    messageHandlers[messageType.toLowerCase()] = {
        requiresAuth : requiresAuth,
        handle: handler
    }
}


module.exports.setup = setupWebsocket;
module.exports.onMessage = registerListener;
module.exports.clientList = userClientPointers;
module.exports.MessageTypes = Message;