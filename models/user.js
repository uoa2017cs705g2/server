const db = require('../db.js');

/**
 * Get a single user object selected by username
 */
module.exports.findByUsername = function(username, done){
    return db.get().query("SELECT id, firstname, lastname, username FROM user WHERE username=?", username, function(err, rows){
        if (err || !rows) return done(err, null);
        if(rows.length > 0){
            done(null, rows[0]);
        }else{
            done(new Error("Not Found"));
        }
    });
}

module.exports.findById = function(id, done){
    return db.get().query("SELECT id, firstname, lastname, username FROM user WHERE id=?", id, function(err, rows){
        if (err || !rows) return done(err, null);
        if(rows.length > 0){
            done(null, rows[0]);
        }else{
            done(new Error("Not Found"));
        }
    });
}

/**
 * Get a all users
 */
module.exports.all = function(done){
    return db.get().query("SELECT id, firstname, lastname, username FROM user", function(err, rows){
        if (err || !rows) return done(err, null);
        if(rows.length > 0){
            done(null, rows);
        }else{
            done(new Error("No Users"));
        }
    });
}

/**
 * Returns True/False based on if the credentials are correct.
 */
module.exports.checkCredentials = function(username, password, done){
    module.exports.login(username, password, (err, result) => {
        done(err, result != null);
    });
}

/**
 * Returns false if incorrect credentials
 * Returns User object if correct.
 */
module.exports.login = function(username, password, done){
    db.get().query("SELECT * FROM user WHERE username=?", username, function(err, rows){
        if(err || rows == undefined) return done(err, null);
        if(rows.length > 0){
            if(rows[0].password === password){
                delete rows[0].password;
                done(null, rows[0]);
            }
            else{
                //Wrong Password
                done(null, false);
            }
        }
        else{
            //No such user
            done(null, false);
        }
    });
}