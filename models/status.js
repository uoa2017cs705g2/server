const db = require('../db.js');
const Relation = require('./relation');
const Friend = require('./friend');
const websocket = require('../websocket');

module.exports.getUserStatus = (userid, done) => {
    return db.get().query("SELECT user_id, status_timestamp, status_id, status, detail_name, detail_value, detail_id FROM status_updates WHERE user_id=? AND \
    status_id = (SELECT id FROM status WHERE user = ? ORDER BY timestamp DESC LIMIT 1)\
    ", [userid, userid], function(err, rows){
        if (err || !rows) return done(err, null);
        if(rows.length > 0){
            var status = {
                highest: "",
                details: []
            };
            rows.forEach(function(row) {
                status.highest = row.status;
                if(row.detail_name != null){
                    var detail = {
                        id: row.detail_id,
                        name: row.detail_name,
                        value: row.detail_value
                    };
                    status.details.push(detail);
                }
            }, this);
            done(null, status);
        }else{
            done(null, {
                highest: "Offline",
                details: []
            });
        }
    });
};


module.exports.setStatus = (uid, status, done) => {
    return db.get().getConnection((err, conn) => {
            //Check that a highest level status has been set.
            var highest_level_status = false;
            if(status.highest != undefined){
                highest_level_status = status.highest;
            }
            else if(status.status != undefined){
                highest_level_status = status.status;
            }
            else{
                return done(new Error("Highest status level not defined!"));
            }


            conn.beginTransaction((err) => {
            db.get().query("INSERT INTO status (user, highest) VALUES (?, ?)", [uid, highest_level_status], (err, result) => {
                if(err) return conn.rollback(() => { done(err) });

                if(status.details != undefined && status.details.length > 0){
                    var insertString = "";
                    var insertValues = [];
                    status.details.forEach(function(detail) {
                        insertString += "(?, ?, ?)";
                        insertValues.push(result.insertId);
                        insertValues.push(detail.id);
                        insertValues.push(detail.value);
                    }, this);
                    db.get().query("INSERT INTO status_detail (status_id, permission_id, value) VALUES " + insertString, insertValues, (err, result1) => {
                        if(err) return conn.rollback(() => { done(err) });
                        conn.commit((err) => {
                            if(err) return conn.rollback(() => { done(err) });
                            done(null, true);
                        });
                    });
                }
                else{
                    //No details, so just commit the status as is.
                    conn.commit((err) => {
                        if(err) return conn.rollback(() => { done(err) });
                        done(null, true);
                    });
                }
            });            
        });
    });
};


module.exports.sendStatusUpdatesToUserRelations = (userId, done) => {
    Relation.getAllRelations(userId, (err, relations) => {
        if(err) return done(err);
        relations.forEach(function(relation) {
            Friend.getStatusForMe(relation.friend, userId, (err, status) => {
                if(websocket.clientList[relation.friend] != undefined){
                    websocket.clientList[relation.friend].sendMessage(websocket.MessageTypes.StatusUpdate.type, {
                        user: userId,
                        status: status
                    });
                }
            });
        }, this);
        done(null);
    });
}