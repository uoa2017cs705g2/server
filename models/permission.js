const db = require('../db.js');

module.exports.getMemberDetailLevel = (member2, member1, done) => {
    var getFriendPerms = (c1) => db.get().query("SELECT permission_id, permission_name, permission_enabled, permission_disabled FROM user_memeber_friendrelation_permission WHERE user_id=? AND friend_id = ?",[member1, member2], function(err, rows){
        if (err || !rows) return c1(err, null);
        c1(null, rows);
    });

    var getContactGroupPerms = (c2) => db.get().query("SELECT permission_id, permission_name, permission_enabled, permission_disabled FROM user_member_contactgrouprelation_permission WHERE user_id=? AND friend_id = ?",[member1, member2], function(err, rows){
        if (err || !rows) return c2(err, null);
        c2(null, rows);
    });

    var getGuildPerms = (c3) => db.get().query("SELECT permission_id, permission_name, permission_enabled, permission_disabled FROM user_member_guildrelation_permission WHERE user_id=? AND friend_id = ?",[member1, member2], function(err, rows){
        if (err || !rows) return c3(err, null);
        c3(null, rows);
    });

    var processHigherRows = (rows) => {
        rows.forEach(function(row) {
            if(row.permission_enabled == 1 && row.permission_disabled == 0){
                perms[row.permission_name] = {
                    id: row.permission_id,
                    allowed: true
                };
            }else{
                perms[row.permission_name] = {
                    id: row.permission_id,
                    allowed: false
                };
            }
        }, this);
    };

    var perms = {};

    return getGuildPerms((err, rows1) => {
        if(err) return console.log(err);
        processHigherRows(rows1);
        getContactGroupPerms((err, rows2) => {
            if(err) return console.log(err);
            processHigherRows(rows2);
            getFriendPerms((err, rows3) => {
                if(err) return console.log(err);
                processHigherRows(rows3);
                done(null, perms);
            });
        });
    });
}

module.exports.getAllPermissionSet = (done) => {
    db.get().query("SELECT * FROM permissionset_permissions", (err, rows) => {
        if(err) return done(err);
        var permissionSets = {};
        
        rows.forEach(function(row) {
            if(permissionSets[row.permissionset_id] == undefined){
                permissionSets[row.permissionset_id] = {
                    id: row.permissionset_id,
                    name: row.permissionset_name,
                    permissions: []
                };
            }

            permissionSets[row.permissionset_id].permissions.push({
                id: row.permission_id,
                name: row.permission_name,
                enabled: row.permission_enabled,
                disabled: row.permission_disabled
            });
        }, this);
        done(null, permissionSets);
    });
};

module.exports.getPermissionSet = (permissionSetId, done) => {
    db.get().query("SELECT * FROM permissionset_permissions WHERE permissionset_id = ?", [permissionSetId], (err, rows) => {
        if(err) return done(err);
        if(rows.length < 1) return done(null, false);
        var permissionSet = {
            id: rows[0].permissionset_id,
            name: rows[0].permissionset_name,
            permissions: []
        };
        rows.forEach(function(row) {
            permissionSet.permissions.push({
                id: row.permission_id,
                name: row.permission_name,
                enabled: row.permission_enabled,
                disabled: row.permission_disabled
            });
        }, this);
        done(null, permissionSet);
    });
};

module.exports.getAll = (done) => {
    db.get().query("SELECT * FROM permission", (err, rows) => {
        if(err) return done(err);
        return done(null, rows);
    });
};