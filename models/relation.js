const db = require('../db.js');
//Get all the relations to the user, (aka, all the members that need status updates when something changes (friend, group, guild))
module.exports.getAllRelations = (userId, done) => {
    //DB 3 commands UNION together into one result set.
    return db.get().query("SELECT friend as `friend` FROM user_contactgroup_relation WHERE user = ? UNION SELECT friend as `friend` FROM user_friend_relation WHERE user = ? UNION SELECT friend_user.id as `friend` FROM user_guild_relation INNER JOIN user as `friend_user` ON user_guild_relation.user = friend_user.id WHERE user_guild_relation.guild IN (SELECT guild FROM user_guild_relation WHERE user = ?)AND user_guild_relation.user != ?"
    , [userId, userId, userId, userId], (err, rows) => {
        if(err) return done(err);
        done(null, rows);
    });
}