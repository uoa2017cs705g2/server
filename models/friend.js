const db = require('../db.js');
const Permission = require('./permission');
const Status = require('./status');

module.exports.getStatusForMe = (myId, friendId, done) => {
    //Find the details flagged as allowed by the database.
    Permission.getMemberDetailLevel(myId, friendId, (err, perms) => {
        if(err) return done(err);
        //Get the actual status + details of the other memeber.
        Status.getUserStatus(friendId, (err, status) => {
            if(err) return done(err);
            //For each status detail, if we are allowed it, then add it to the status object
            var statusObject = {
                highest: status.highest,
                details: []
            };
            status.details.forEach(function(detail) {
                if(perms[detail.name] != undefined && perms[detail.name].allowed == true){
                    statusObject.details.push(detail);
                }
            }, this);
            done(null, statusObject);
        });
    });
};

module.exports.getFriend = (myId, friendId, done) => {
    return db.get().query("SELECT * FROM friend_users WHERE user_id = ? AND friend_id = ?", [myId, friendId], function(err, rows){
        if (err || !rows) return done(err, null);
        if(rows.length > 0){
            done(null, rows[0]);
        }else{
            done(new Error("Not Found"));
        }
    });
};

module.exports.getFriends = (myId, done) => {
    return db.get().query("SELECT * FROM friend_users WHERE user_id = ?", [myId], function(err, rows){
        if (err || !rows) return done(err, null);
        if(rows.length > 0){
            done(null, rows);
        }else{
            done(new Error("Not Found"));
        }
    });
};

module.exports.removeFriend = (myId, friendId, done) => {
    return db.get().query("DELETE FROM user_friend_relation WHERE user = ? AND friend = ?", [myId, friendId], (err, result) => {
        if(err) return done(err);
        done(null, result);
    });
};

module.exports.addFriend = (myId, friendId, permissionSetId, done) => {
    return db.get().query("INSERT INTO user_friend_relation (user, friend, permission_set) VALUES (?, ?, ?)", [myId, friendId, permissionSetId], (err, result) => {
        if(err) return done(err);
        done(null, result);
    });
};