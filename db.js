var mysql = require('mysql')

var state = {
pool: null,
}

exports.connect = function(done) {
state.pool = mysql.createPool({
    user     : 'root',
    password : 'root',
    database : 'statusUpdater',
    socketPath: '/Applications/MAMP/tmp/mysql/mysql.sock',
});

done()
}

exports.get = () => { return state.pool }

