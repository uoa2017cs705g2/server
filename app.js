//Imports
const express     = require('express');
const WebSocket   = require('ws');
const http        = require('http');
const mysql       = require('mysql');
const passport    = require('passport');
const pphttp      = require('passport-http');

const db          = require('./db');
const User        = require('./models/user');
const socket      = require('./websocket');
const bodyParser  = require('body-parser');
const cors        = require('cors');

//HTTP & Websocket stuff
var port = process.env.PORT || 3000;
var app = express();
var server = http.createServer(app);
var wss = new WebSocket.Server({ server });
socket.setup(wss);

//Auth stuff
const BasicStrategy = pphttp.BasicStrategy;
passport.use(new BasicStrategy((username, password, done) => {
        User.login(username, password, done);     
}));

//Seutp MySQL connection.
db.connect(() => {console.log("Connected to DB")});

//Setup BodyParser for post requests.
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//Allow CORs
app.use(cors());

//HTTP Routes
app.get('/', function (req, res) {
    res.send("Hello World!");
});

app.get('/protected', passport.authenticate('basic', { session: false }), function(req, res) {
    res.send("If you are seeing this, then you are authenticated! Yay!");
});

app.use("/user", require('./controllers/userController'));
app.use("/guild", require('./controllers/guildController'));
app.use("/group", require('./controllers/groupController'));
app.use("/friend", require('./controllers/friendController'));
app.use("/status", require('./controllers/statusController'));
app.use("/startup", require("./controllers/startupController"));
app.use("/detail", require('./controllers/detailController'));
app.use("/detailSet", require('./controllers/permissionSetController'));

//Websocket message handlers
socket.onMessage("test", false, (ws, message) => {
    ws.sendMessage("test", {
        message: "Socket appears to be working!",
        username: ws.user.username,
        firstname: ws.user.firstname,
        lastname: ws.user.lastname
    });
    console.log(ws.user); //User object, useful for getting user id, etc.
    console.log(wss.clients);
});

//==== Start the Server ===//
var server = server.listen(port, function () {
    console.log('Status API listening on port ' + port)
})
