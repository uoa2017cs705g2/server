var express = require('express')
, router = express.Router()
, passport = require('passport')
const User = require('../models/user');

//Get User
router.get('/:id', passport.authenticate('basic', { session: false }), function(req, res) {
    User.findByUsername(req.params.id, (err, user) => {
        if(err){
            res.sendStatus(404); 
            return;
        }
        res.json(user);
    })
});

module.exports = router