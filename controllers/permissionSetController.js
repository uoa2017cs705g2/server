var express = require('express')
, router = express.Router()
, passport = require('passport');
const Permission = require('../models/permission');

//Get all permission sets
router.get('/', passport.authenticate('basic', { session: false }), function(req, res) {
    Permission.getAllPermissionSet((err, permissionSets) => {
        if(err) return res.sendStatus(500);
        res.json(permissionSets);
    });
});

//Get all details
router.get('/:id', passport.authenticate('basic', { session: false }), function(req, res) {
    Permission.getPermissionSet(req.params.id, (err, permissionSet) => {
        if(err) return res.sendStatus(500);
        if(!permissionSet) return res.sendStatus(404);
        res.json(permissionSet);
    });
});

module.exports = router