var express = require('express')
, router = express.Router()
, passport = require('passport');

//Get my groups
router.get('/', passport.authenticate('basic', { session: false }), function(req, res) {
    res.sendStatus(501);
});

//Get Group
router.get('/:id', passport.authenticate('basic', { session: false }), function(req, res) {
    res.sendStatus(501);
});

//Update Group Settings
router.post('/:id', passport.authenticate('basic', { session: false }), function(req, res) {
    res.sendStatus(501);
});

//Add new user to the group
router.put('/:id/add/:userid', passport.authenticate('basic', { session: false }), function(req, res) {
    res.sendStatus(501);
});

//Remove user from the group
router.delete('/:id/remove/:userid', passport.authenticate('basic', { session: false }),function(req, res) {
    res.sendStatus(501);
});

module.exports = router