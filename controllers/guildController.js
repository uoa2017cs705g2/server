var express = require('express')
, router = express.Router()
, passport = require('passport');

//Get my guilds
router.get('/', passport.authenticate('basic', { session: false }), function(req, res) {
    res.sendStatus(501);
}); 

//Get Guild
router.get('/:id', passport.authenticate('basic', { session: false }), function(req, res) {
    res.sendStatus(501);
});

//Update Guild Settings
router.post('/:id', passport.authenticate('basic', { session: false }), function(req, res) {
    res.sendStatus(501);
});

//add a new user to the guild
router.put('/:id/add/:userid', passport.authenticate('basic', { session: false }), function(req, res) {
    res.sendStatus(501);
});

//Remove a user from the guild
router.delete('/:id/remove/:userid', passport.authenticate('basic', { session: false }), function(req, res) {
    res.sendStatus(501);
});

module.exports = router