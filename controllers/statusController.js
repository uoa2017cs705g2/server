var express = require('express')
, router = express.Router()
, passport = require('passport');
const Status = require('../models/status');

//Get my current status for all groups, contacts, etc. (maybe?)
router.get('/', passport.authenticate('basic', { session: false }), function(req, res) {
    Status.getUserStatus(req.user.id, (err, status) => {
        res.json(status);
    });
});

//alter personal status
router.post('/', passport.authenticate('basic', { session: false }), function(req, res) {
    Status.setStatus(req.user.id, req.body, (err, result) => {
        if(err) return res.sendStatus(500);
        res.sendStatus(200);
        Status.sendStatusUpdatesToUserRelations(req.user.id, () => {
            console.log("Updates finished sending for: " + req.user.id);
        });
    });
});


module.exports = router