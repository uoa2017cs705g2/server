var express = require('express')
, router = express.Router()
, passport = require('passport');
const Permission = require('../models/permission');

//Get all details
router.get('/', passport.authenticate('basic', { session: false }), function(req, res) {
    Permission.getAll((err, details) => {
        if(err) return res.sendStatus(500);
        res.json(details);
    });
});

module.exports = router