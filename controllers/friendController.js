var express = require('express')
, router = express.Router()
, passport = require('passport');
const Friend = require('../models/friend');

//Get my Friends objects
router.get('/', passport.authenticate('basic', { session: false }), function(req, res) {
    Friend.getFriends(req.user.id, (err, friends) => {
        if(err) return res.sendStatus(500);
        res.json(friends);
    });
});

//Get Friend object
router.get('/:id', passport.authenticate('basic', { session: false }), function(req, res) {
    Friend.getFriend(req.user.id, req.params.id, (err, friend) => {
        if(err) return res.sendStatus(500);
        res.json(friend);
    });
});

//alter Friend settings
router.post('/:id', passport.authenticate('basic', { session: false }), function(req, res) {
    res.sendStatus(501);
});

//Get Friend Status object only.
router.get('/:id/status', passport.authenticate('basic', { session: false }), function(req, res) {
    Friend.getStatusForMe(req.user.id, req.params.id, (err, status) => {
        res.json(status);
    });
});

//Attempt to add new friend (let them see you!)
router.put('/:id', passport.authenticate('basic', { session: false }), function(req, res) {
    if(req.body.permission_set == undefined) return res.sendStatus(400);
    Friend.addFriend(req.user.id, req.params.id, req.body.permission_set, (err, result) => {
        if(err) return res.sendStatus(500);
        return res.sendStatus(200);
    });
});

//Remove a contact (dont let someone else see you!)
router.delete('/:id', passport.authenticate('basic', { session: false }), function(req, res) {
    //Attempt to add new friend (let them see you!)
    Friend.removeFriend(req.user.id, req.params.id, (err, result) => {
        if(err) return res.sendStatus(500);
        return res.sendStatus(200);
    });
});

module.exports = router