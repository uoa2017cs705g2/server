var express = require('express')
, router = express.Router()
, passport = require('passport');

//Send a user all the information that they need when they open the client.
router.get('/', passport.authenticate('basic', { session: false }), function(req, res) {
    res.sendStatus(501);
});

module.exports = router