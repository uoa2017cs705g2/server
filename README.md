# Status Gauge Server
NodeJs server that handles the management of status gauges information. Specifically, this server handles the management of users, groups, guild and the propergation of status updates. Status updates are posted to this server using HTTP or a WebSocket connection, the server decides a new state for the user and propergates the chances to the various different levels of contacts.

# Install & Run
1. `cd /path/to/installation/folder`
2. `git clone git clone https://username@bitbucket.org/uoa2017cs705g2/server.git .`
3. `npm install`
4. `npm start`

# Credits
- Jack Haller <@aucklanduni.ac.nz>
- Ryan Tiedemann <rtie478@aucklanduni.ac.nz>
- Jay Pandya <@aucklanduni.ac.nz>
- Priyankit Singh <@aucklanduni.ac.nz>